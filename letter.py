from keras.models import Sequential
from keras.layers import Dense,Activation
import numpy

numpy.random.seed(7)

dataset = numpy.loadtxt("data.csv", delimiter=",")
DataInput= dataset[:,1:17]
DataOutput = dataset[:,0]

vector = []
for i in range(len(DataOutput)):
	newllist = [0] * 26
	newllist[int(DataOutput[i]-65)] = 1
	vector.append(newllist)

model = Sequential()
model.add(Dense(200, input_dim=16, init='uniform'))
model.add(Activation('softmax'))
model.add(Dense(60, init='uniform'))
model.add(Activation('softmax'))
model.add(Dense(26, init='uniform'))
model.add(Activation('softmax'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

x1=numpy.array(DataInput)
y1=numpy.array(DataOutput)

model.fit(x1, vector, nb_epoch=5, batch_size=10)

scor = model.evaluate(DataInput, vector)
print('  ', scor[1]*100)
